turtles-own
  [ state                ;; 1: healthy, 2: sick, 3: treated, 4: dead; 5 recovered
    social-distance?     ;; if the turtle do social distance
    sick-time            ;; how long, in weeks, the turtle has been infectious
  ]
globals
  [ %infected             ;; what % of the population is infectious (state 2 and 3)
    %treated              ;; what % of the population is treated (state 3)
    %dead                 ;; what % of the population is dead (state 4)
    %recovered            ;; what % of the population is recovered (state 5)
    %medical-availability ;; what % of total medical facility capacity is still available
    carrying-capacity    ;; the number of turtles that can be in the world at one time
  ]

;; The setup is divided into four procedures
to setup
  clear-all
  setup-constants
  setup-turtles
  update-global-variables
  update-display
  reset-ticks
end

;; We create a variable number of turtles of which 1 are infectious,
;; and distribute them randomly
to setup-turtles
  create-turtles number-people
    [ setxy random-xcor random-ycor
      set sick-time 0
      ifelse random-float 100 < social-distancing [ set social-distance? true] [ set social-distance? false]
      set size 1.2  ;; easier to see
      get-healthy ]
  ask n-of 1 turtles
    [ get-sick ]
end

to get-sick ;; turtle procedure
  set state 2
end

to get-healthy ;; turtle procedure
  set state 1
  set sick-time 0
end

;; This sets up basic constants of the model.
to setup-constants
  set carrying-capacity 500 ;; maximum number of population can be set in this model
end

to go
  ask turtles [
    get-older
    if (not (state = 4) and not (social-distance?)) [ move ]
    if (state = 2 and sick-time >= incubation-period * 2) [ check-for-treatment ]
    if (state = 2 or state = 3) [ recover-or-die ]
    if (state = 2 or state = 3) [ infect ]
  ]
  update-global-variables
  update-display
  tick
end

to update-global-variables
  if count turtles > 0
    [ set %infected (count turtles with [ state = 2 or state = 3 ] / count turtles) * 100
      set %treated (count turtles with [ state = 3 ] / count turtles) * 100
      set %dead (count turtles with [ state = 4 ] / count turtles) * 100
      set %recovered (count turtles with [ state = 5 ] / count turtles) * 100
      set %medical-availability ((medical-capacity - count turtles with [ state = 3 ] ) / medical-capacity) * 100
  ]
end

to update-display
  ask turtles
    [ if shape != turtle-shape [ set shape turtle-shape ]
      set color ifelse-value (state = 1) [ green ] [ ifelse-value (state = 2) [ red ] [ ifelse-value (state = 3) [ yellow ] [ ifelse-value (state = 4) [ gray ] [ blue ] ] ] ] ]
end

to get-older ;; turtle procedure
  if (state = 2 or state = 3) [ set sick-time sick-time + 1 ]
end

;; Check for Turtle if it can get the treatment, considering the remaining capacity of medical facility
to check-for-treatment
  if (count turtles with [ state = 3 ] < medical-capacity) [
    if random-float 100 < chance-of-getting-treatment [ set state 3 ]
  ]
end

;; Turtles move about at random.
to move ;; turtle procedure
  rt random 100
  lt random 100
  fd 1
end

;; If a turtle is sick, it infects other turtles on the same patch.
to infect ;; turtle procedure
  ask other turtles-here with [ state = 1 ]
    [ if random-float 100 < infectiousness [ get-sick ] ]
end

;; Once the turtle has been sick long enough, it may pass away, recover or stay sick.
to recover-or-die ;; turtle procedure
  if sick-time > incubation-period * 2                     ;; If the turtle has survived past the virus' incubation period, then
    [ if state = 2 [                                       ;; if it's untreated,
          if random-float 100 < chance-death [ pass-away ] ;; the chance he dies
          if random-float 100 < chance-recover [ recover ] ;; the chance he recovers
      ]
      if state = 3 [                                       ;; if it's treated,
          if random-float 100 < chance-death-when-treated [ pass-away ] ;; the chance he dies
          if random-float 100 < chance-recover-when-treated [ recover ] ;; the chance he recovers
      ]
  ]
end

to recover
  set state 5
end

to pass-away
  set state 4
end

to startup
  setup-constants ;; so that carrying-capacity can be used as upper bound of number-people slider
end
@#$#@#$#@
GRAPHICS-WINDOW
280
10
778
509
-1
-1
14.0
1
10
1
1
1
0
1
1
1
-17
17
-17
17
1
1
1
ticks
30.0

SLIDER
40
170
265
203
incubation-period
incubation-period
0.0
50
12.0
1.0
1
days
HORIZONTAL

SLIDER
40
250
265
283
chance-recover
chance-recover
0.0
99.0
5.0
1.0
1
%
HORIZONTAL

SLIDER
40
130
265
163
infectiousness
infectiousness
0.0
99.0
80.0
1.0
1
%
HORIZONTAL

BUTTON
280
515
350
550
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
355
515
426
550
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
0

PLOT
785
10
1180
290
Populations
days/2
people
0.0
365.0
0.0
500.0
false
true
"" ""
PENS
"healthy" 1.0 0 -13840069 true "" "plot count turtles with [ state = 1 ]"
"sick not treated" 1.0 0 -2674135 true "" "plot count turtles with [ state = 2 ]"
"treated" 1.0 0 -1184463 true "" "plot count turtles with [ state = 3 ]"
"dead" 1.0 0 -7500403 true "" "plot count turtles with [ state = 4 ]"
"recovered" 1.0 0 -13345367 true "" "plot count turtles with [ state = 5 ]"
"medical capacity" 1.0 0 -16777216 true "" "plot medical-capacity"

SLIDER
40
30
265
63
number-people
number-people
10
carrying-capacity
400.0
1
1
NIL
HORIZONTAL

MONITOR
785
295
870
340
NIL
%infected
2
1
11

MONITOR
785
345
870
390
NIL
%treated
2
1
11

MONITOR
705
515
779
560
days
ticks / 2
1
1
11

CHOOSER
440
515
585
560
turtle-shape
turtle-shape
"person" "circle"
0

SLIDER
40
310
265
343
medical-capacity
medical-capacity
0
carrying-capacity
30.0
1
1
NIL
HORIZONTAL

SLIDER
40
350
265
383
chance-of-getting-treatment
chance-of-getting-treatment
0
100
90.0
1
1
%
HORIZONTAL

SLIDER
40
390
265
423
chance-recover-when-treated
chance-recover-when-treated
0
99
30.0
1
1
%
HORIZONTAL

SLIDER
40
430
265
463
chance-death-when-treated
chance-death-when-treated
0
99
5.0
1
1
%
HORIZONTAL

SLIDER
40
210
265
243
chance-death
chance-death
0
99
10.0
1
1
%
HORIZONTAL

SLIDER
40
70
265
103
social-distancing
social-distancing
0
99
80.0
1
1
%
HORIZONTAL

TEXTBOX
40
295
190
313
Medical Treatment Profile
11
0.0
1

TEXTBOX
40
115
190
133
Virus Profile
11
0.0
1

TEXTBOX
40
15
190
33
Society Profile
11
0.0
1

MONITOR
875
345
955
390
NIL
%dead
2
1
11

MONITOR
875
295
955
340
NIL
%recovered
2
1
11

MONITOR
785
395
955
440
% Availability of medical facility
%medical-availability
2
1
11

@#$#@#$#@
## WHAT IS IT?

This model simulates the transmission of a virus in a human population, and how medical treatment and social distancing affect the spreading.

## HOW IT WORKS

The model is initialized with 300 people, of which 1 are infected.  People belong to one of five states: healthy but susceptible to infection (green), sick and not treated (red), sick but treated (yellow), dead (grey) and recovered (blue). People move randomly around the world except the dead. Sick people (red and yellow) may infect healthy people (green). The virus has incubation period (period between the person first gets infected and the first symptom occurs). After the incubation period, people may die of infection or recover. When the first symptom occurs, they may get a medical treatment (change from red to yellow), depending on the capacity of the medical facility.

Some of these factors are summarized below with an explanation of how each one is treated in this model.

### The density of the population

Population density affects how often infected and susceptible individuals come into contact with each other. You can change the size of the initial population through the NUMBER-PEOPLE slider.

### Social distancing

Determines how mobile the society is. 0 means everyone moves around the world, while 100 means everyone stay in his own space.

### Infectiousness (or transmissibility)

How easily does the virus spread?  Some viruses with which we are familiar spread very easily.  Some viruses spread from the smallest contact every time.  Others (the HIV virus, which is responsible for AIDS, for example) require significant contact, perhaps many times, before the virus is transmitted.  In this model, infectiousness is determined by the INFECTIOUSNESS slider.

### Incubation period

How long is a person infected before they either recover or die?  This length of time is essentially the virus's window of opportunity for transmission to new hosts. In this model, incubation period is determined by the incubation-period slider.

### Chance of death and Chance to recover

Sick person may die, recover or stay sick. Chance of death determines the probability of a sick person to die. Chance to recover determines the probability of a sick person to recover.

### Medical capacity and Chance of getting treatment

Medical facility's capacity to accept people for treatment is limited. If the facility is full, people will be left untreated. Even if the facility is not full, there may be several reasons why somebody is not treated.

### Chance of death and Chance to recover when treated

People will generally have a higher chance to recover and lower chance of death if they get proper medical treatment.

## HOW TO USE IT

Each "tick" represents a half day (12 hours) in the time scale of this model.

The SOCIAL-DISTANCING slider determine how massive is the scale of social distancing program.

The INFECTIOUSNESS slider determines how great the chance is that virus transmission will occur when an infected person and susceptible person occupy the same patch.  For instance, when the slider is set to 50, the virus will spread roughly once every two chance encounters.

The INCUBATION-PERIOD slider determines the number of days before an infected person show the first symptom, and have a chance to die or to recover.

The CHANCE-DEATH slider controls the likelihood that an infection will end in death. When this slider is set at zero, people will stay sick.

The CHANCE-RECOVER slider controls the likelihood that an infection will end in recovery. When this slider is set at zero, people will stay sick.

The MEDICAL-CAPACITY slider determines the number of people that can get medical treatment at the same time.

The CHANCE-OF-GETTING-TREATMENT slider controls the likelihood that somebody will get medical treatment, provided that the capacity is still adequate.

The CHANCE-DEATH-WHEN-TREATED slider controls the likelihood that an infection will end in death when someone is in treatment. When this slider is set at zero, people will stay sick.

The CHANCE-RECOVER-WHEN-TREATED slider controls the likelihood that an infection will end in recovery when someone is in treatment. When this slider is set at zero, people will stay sick.

The SETUP button resets the graphics and plots and randomly distributes NUMBER-PEOPLE in the view. All but 1 of the people are set to be green susceptible people and 1 red infected people (of randomly distributed ages).  The GO button starts the simulation and the plotting function.

The TURTLE-SHAPE chooser controls whether the people are visualized as person shapes or as circles.

Output monitors show the percent of the population that is sick, treated, recovered and dead, and the number of days that have passed.  The plot shows (in their respective colors) the number of healthy (susceptible), sick, treated, dead and recovered people. It also shows the capacity of medical facility in black.

## EXTENDING THE MODEL

Add lockdown feature.

Add minimum period of medical treatment before somebody recovers.

## CREDITS

This model is made by Natalius, S. (2020)

Inspiration to make this model comes from the following article: https://www.washingtonpost.com/graphics/2020/world/corona-simulator/

This model is based on the NetLogo Virus model by Wilensky, U. (1998).
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.1.1
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
1
@#$#@#$#@
