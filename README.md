# netlogo-virus-social-distancing

### How to run the model?

The model is created using NetLogo. Run NetLogo application (can be downloaded from its [webpage](https://ccl.northwestern.edu/netlogo/)) and open `Virus-social-distancing.nlogo`. 

### Requirements

- A person may have 5 states: healthy, sick, treated, dead, recovered
- The simulation world has N people.
- Social distancing rate determines how many people move around and how many are not. 0% means all people move. 100% means all people don't move.
- Start with 1 person sick.
- If one healthy person is in contact with another person, there is p% probability the other person get sick too (infectiousness).
- The medical facility is present with capacity of C people.
- There is an incubation period of the disease (Di). During incubation period, nothing happens to the sick people, but they can infect others.
- For sick people, after the incubation period, if the number of people treated (T) is lower than C, then there is q% probability that he be treated (q is close to 100). If T is higher than C, then he won't be treated.
- When incubation period is over:
    - If people is in treated state, there is r% chance of death and s% chance of recovery
    - If people is in sick state, there is u% chance of death and v% chance of recovery
    - r << u , s >> v
- If a person is dead, he stops moving around and is not able to infect others.
- If a person recovers, he cannot get infected again.